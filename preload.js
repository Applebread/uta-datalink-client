const { contextBridge, ipcRenderer } = require('electron/renderer')
const fsuipc = require("fsuipc");

contextBridge.exposeInMainWorld('electronAPI', {
    getFSUIPCData: async () => {
        try {
            const obj = new fsuipc.FSUIPC();
            await obj.open();

            // Добавляем оффсеты (FSUIPC)
            obj.add('latitude', 0x0560, fsuipc.Type.Int64);
            obj.add('longitude', 0x0568, fsuipc.Type.Int64);
            obj.add('heading', 0x0580, fsuipc.Type.Int64);
            obj.add('magVar', 0x02A0, fsuipc.Type.Int16);
            obj.add('bank', 0x057C, fsuipc.Type.Int32);
            obj.add('pitch', 0x0578, fsuipc.Type.Int32);
            obj.add('vs', 0x02C8, fsuipc.Type.Int32);
            obj.add('gForce', 0x11BA, fsuipc.Type.Int16);
            obj.add('altitude', 0x3324, fsuipc.Type.Int32);
            obj.add('seaLevelAltitude', 0x0590, fsuipc.Type.Int32);
            obj.add('acType', 0x3D00, fsuipc.Type.String, 256);
            obj.add('acTypeICAO', 0x3160, fsuipc.Type.String, 5);
            obj.add('landingLightsStatus', 0x028C, fsuipc.Type.UInt16);
            obj.add('seatbeltSign', 0x341D, fsuipc.Type.UInt16);
            obj.add('fuelTotalQuantityWeightPounds', 0x126C, fsuipc.Type.Int32);

            // Читаем значения
            const rawData = await obj.process();
            const magVar = ((rawData.magVar * 360) / 65536).toFixed(2);

            // Преобразуем в читаемый формат
            const readableData = {
                latitude: (rawData.latitude * 90) / (10001750 * 65536 * 65536),
                longitude: (rawData.longitude * 360) / (65536 * 65536 * 65536 * 65536),
                magVar: magVar,
                heading: ((rawData.heading * 360) / (65536 * 65536)).toFixed(2) - magVar,
                bank: (rawData.bank * 360 / (65536 * 65536)).toFixed(2),
                pitch: (rawData.pitch * 360 / (65536 * 65536)).toFixed(2),
                verticalSpeed: (rawData.vs / 256).toFixed(2),
                gForce: (rawData.gForce / 624).toFixed(2),
                altitude: rawData.altitude,
                seaLevelAltitude: rawData.seaLevelAltitude,
                aircraftType: rawData.acType.trim(),
                acTypeICAO: rawData.acTypeICAO.trim(),
                landingLights: !!rawData.landingLightsStatus,
                seatbeltSign: (rawData.seatbeltSign & 0xFF) === 1,
                fuelTotalQuantityWeightPounds: rawData.fuelTotalQuantityWeightPounds,
                fuelTotalQuantityWeightKg: (rawData.fuelTotalQuantityWeightPounds / 2.205).toFixed(2)
            };
            ipcRenderer.send('fsuipc-data', readableData);

            await obj.close();
            return readableData;
        } catch (error) {
            console.error('FSUIPC Error:', error);
            return null;
        }
    },

    appendToCsv: (fileName, data) => ipcRenderer.send('append-to-csv', fileName, data)
})
