export const environment = {
    production: true,
    apiEndpoint: "http://185.112.147.106:8083",
    imagesBasePath: "http://185.112.147.106:8083",
    simbriefApiUrl: "https://api.simbrief.com",
    simbriefToken: "SIMBRIEF_API_TOKEN",
    mapboxApiKey: "MAPBOX_API_KEY"
};
