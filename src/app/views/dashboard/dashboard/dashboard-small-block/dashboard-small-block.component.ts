import {Component, Inject, Input} from '@angular/core';
import {APP_CONFIG, AppConfig} from "../../../../app-config/app-config.module";

@Component({
  selector: 'app-dashboard-small-block',
  templateUrl: './dashboard-small-block.component.html',
  styleUrls: ['./dashboard-small-block.component.scss']
})
export class DashboardSmallBlockComponent {
  @Input() public title: string = '';
  @Input() public value: string | null = null;
  @Input() public icon: string = '';
  @Input() public image: string | null = null;
  @Input() public helpLink: string = '';
  @Input() public helpText: string = '';

  constructor(@Inject(APP_CONFIG) private config: AppConfig) {
  }

  public backendImagePath(): string {
    return this.config.imagesBasePath;
  }
}
