import {Component, Inject, OnInit} from '@angular/core';
import {LogbookInterface, LogbookService} from "../../../services/logbook/logbook.service";
import {APP_CONFIG, AppConfig} from "../../../app-config/app-config.module";
import {AuthService} from "../../../services/auth/auth.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  logbook: LogbookInterface | null = null

  constructor(private logbookService: LogbookService, @Inject(APP_CONFIG) private config: AppConfig, private authService: AuthService) {
  }

  async ngOnInit(): Promise<void> {
    await this.authService.updateAuthInfo(true)

    if (this.userIsPilot()) {
      this.logbookService.getLogbook().subscribe((logbook: LogbookInterface) => {
        this.logbook = logbook;
      });
    }
  }

  nonPilotLogbook(): LogbookInterface {
    return this.logbookService.getLogbookFromLoggedUser();
  }

  public userIsPilot(): boolean {
    return this.authService.loggedInUser?.roles.includes('ROLE_PILOT') || false
  }
}
