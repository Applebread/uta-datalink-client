import {Component, Input} from '@angular/core';
import {LogbookInterface} from "../../../../services/logbook/logbook.service";

@Component({
  selector: 'app-dashboard-user',
  templateUrl: './dashboard-user.component.html',
  styleUrls: ['./dashboard-user.component.scss']
})
export class DashboardUserComponent {

  @Input() public logbook?: LogbookInterface
}
