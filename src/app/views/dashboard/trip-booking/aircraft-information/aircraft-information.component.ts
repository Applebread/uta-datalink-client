import {Component, Input} from '@angular/core';
import {ITrip} from "../../../../services/booking/booking.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-aircraft-information',
  templateUrl: './aircraft-information.component.html',
  styleUrls: ['./aircraft-information.component.scss']
})
export class AircraftInformationComponent {
    @Input() tripInfo?: ITrip

    constructor(private modalService: NgbModal) {
    }

    showAircraftStats(content: any) {
        this.modalService.open(content, { centered: true, size: 'lg' })
    }
}
