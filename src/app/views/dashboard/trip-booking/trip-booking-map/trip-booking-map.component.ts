import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ITrip, ITripLeg} from "../../../../services/booking/booking.service";
import {MapboxService} from "../../../../services/mapbox/mapbox.service";

@Component({
    selector: 'app-trip-booking-map',
    templateUrl: './trip-booking-map.component.html',
    styleUrls: ['./trip-booking-map.component.scss']
})
export class TripBookingMapComponent implements OnChanges {
    @Input() tripInfo?: ITrip;
    private mapInitialized: boolean = false;

    constructor(private mapboxService: MapboxService) {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (!this.tripInfo) return;

        const legs = this.tripInfo.legs.map((leg: ITripLeg) => {
            return {
                from: {
                    latitude: parseFloat(leg.from.latitude),
                    longitude: parseFloat(leg.from.longitude),
                },
                to: {
                    latitude: parseFloat(leg.to.latitude),
                    longitude: parseFloat(leg.to.longitude),
                }
            };
        });

        if (!this.mapInitialized) {
            this.mapboxService.initializeMap(legs)
            this.mapInitialized = true;

            return;
        }

        this.mapboxService.updateMapMarkersAndRoute(legs);
    }
}
