import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BookingService, ITrip, ITripLeg} from "../../../../../services/booking/booking.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AirportsService, IAirport} from "../../../../../services/airport/airports.service";
import {IFlight} from "../../../../../services/timetable/timetable.service";

@Component({
  selector: 'app-trip-leg',
  templateUrl: './trip-leg.component.html',
  styleUrls: ['./trip-leg.component.scss']
})
export class TripLegComponent implements OnInit {
    @Input()  leg?: ITripLeg;
    @Input()  index: number = 0;
    @Input()  tripInfo?: ITrip;
    @Output() tripUpdated: EventEmitter<ITrip> = new EventEmitter<ITrip>();

    availableAlternates:Array<IAirport> = [];
    selectedAlternates:Array<IAirport> = [];

    constructor(public modalService: NgbModal, private airportService: AirportsService, private bookingService: BookingService) {}

    ngOnInit(): void {
        if (!this.leg) return;

        this.airportService.alternatesList(this.leg.to.icao).subscribe((alternates: Array<IAirport>) => {
            if (!this.leg) return;
            this.availableAlternates = alternates;
        })
    }

    toggleSelected(airport: IAirport): void {
        if (!this.selectedAlternates.includes(airport)) {
            this.selectedAlternates.push(airport);

            return;
        }

        this.selectedAlternates.splice(this.selectedAlternates.findIndex((needle: IAirport) => needle.id === airport.id));
    }

    openSelectAlternatesModal(content: any): void {
        if (!this.leg) return;

        this.airportService.alternatesList(this.leg.to.icao).subscribe((alternates: Array<IAirport>) => {
            if (!this.leg) return;
            this.availableAlternates = alternates;
            this.leg.selectedAlternates = [];
            this.selectedAlternates = [];

            this.modalService.open(content, { centered: true, size: 'sm' })
        })
    }

    getAlternateDetailsByIcao(icao: string): IAirport {
        const res = this.availableAlternates.find((airport: IAirport)=> airport.icao === icao);

        if (!res) {
            throw new Error("Detail not found");
        }

        return res;
    }

    saveAlternates(): void {
        if (!this.leg || !this.tripInfo) return;
        const alternates = this.selectedAlternates.map((alternate: IAirport) => alternate.icao);

        this.bookingService.updateTripLegAlternates(this.tripInfo, this.leg, {alternates}).subscribe((trip: ITrip) => {
            this.tripUpdated.emit(trip);
        });
    }

    openSelectArrivalModal(content: any) {
        this.modalService.open(content, { centered: true, size: 'lg' });
    }

    addNewLeg(flight: IFlight) {
        if (!this.tripInfo) return;

        this.bookingService.addNewLeg(this.tripInfo, flight).subscribe((trip: ITrip) => {
            this.tripUpdated.emit(trip);
        })
    }
}
