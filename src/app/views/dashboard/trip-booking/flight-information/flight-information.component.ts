import {Component, Input} from '@angular/core';
import {ITrip, ITripLeg} from "../../../../services/booking/booking.service";

@Component({
  selector: 'app-flight-information',
  templateUrl: './flight-information.component.html',
  styleUrls: ['./flight-information.component.scss']
})
export class FlightInformationComponent {
    @Input() tripInfo?: ITrip

    getFullRouteString(): string {
        if (!this.tripInfo) {
            throw new Error("Trip info do not loaded yet");
        }

        let route = ''

        this.tripInfo.legs.forEach((leg: ITripLeg, key: number)=> {
            if (!this.tripInfo) return;
            if (!key) {
                route += leg.from.icao + ' -> ' + leg.to.icao;
                return;
            }

            route += ' -> ' + leg.to.icao;
        })

        return route;
    }
}
