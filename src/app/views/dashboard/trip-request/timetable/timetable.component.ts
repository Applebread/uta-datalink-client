import {Component, OnInit} from '@angular/core';
import {DayOfWeek, IFlight, ITimetable, TimetableService} from "../../../../services/timetable/timetable.service";
import {IColumn, IPaginationEvent} from "../../../../components/pagable-table/pagable-table.component";
import {ActivatedRoute, Router} from "@angular/router";
import {ITimetableFilterValues} from "./timetable-filter/timetable-filter.component";
import {ITypeRating, LogbookInterface, LogbookService} from "../../../../services/logbook/logbook.service";
import {IAircraftType} from "../../../../services/aircraft/aircraft.service";
import {BookingService} from "../../../../services/booking/booking.service";
import {NgbModal, NgbModalRef, NgbTimepickerConfig} from "@ng-bootstrap/ng-bootstrap";
import {AppToastService} from "../../../../services/toast/app.toast.service";

@Component({
    selector: 'app-timetable',
    templateUrl: './timetable.component.html',
    styleUrls: ['./timetable.component.scss']
})
export class TimetableComponent implements OnInit {
    public timetable: ITimetable | null = null
    public from?: string;
    public to?: string;
    public dateOfFlight?: string;
    public aircraftType?: string;
    public availableForMe?: boolean;
    public logbook?: LogbookInterface
    public page: number = 1;
    public pageSize: number = 15;
    public doBookingModal: NgbModalRef | null = null;
    public bookingFlight?: IFlight;
    public pilotAircraftTypes: Array<IAircraftType> = [];
    public selectedDepartureTime?: any
    public selectedAircraftType?: any;
    bookingDisabled: boolean = false;
    public tableColumns: Array<IColumn> = [
        {
            key: 'flightNumber',
            title: 'Номер рейса',
        },
        {
            key: 'from',
            title: 'Из',
        },
        {
            key: 'to',
            title: 'В',
        },
        {
            key: 'etd',
            title: 'Время вылета',
        },
        {
            key: 'scheduledSince',
            title: 'Доступен с'
        },
        {
            key: 'scheduledTill',
            title: 'Доступен до'
        },
        {
            key: 'daysOfWeek',
            title: 'Дни недели'
        },
        {
            key: 'aircraftType',
            title: 'Тип ВС'
        }
    ];

    public daysOfWeek: Array<DayOfWeek> = [
        {key: 1, value: 'Пн'},
        {key: 2, value: 'Вт'},
        {key: 3, value: 'Ср'},
        {key: 4, value: 'Чт'},
        {key: 5, value: 'Пт'},
        {key: 6, value: 'Сб'},
        {key: 7, value: 'Вс'},
    ];

    constructor(
        private service: TimetableService,
        private route: ActivatedRoute,
        private router: Router,
        private readonly logbookService: LogbookService,
        private readonly bookingService: BookingService,
        private readonly modalService: NgbModal,
        private toastService: AppToastService,
        timePickerConfig: NgbTimepickerConfig
    ) {
        timePickerConfig.seconds = false;
        timePickerConfig.spinners = false;
    }

    isDayIn(days: Array<string>, day: number): boolean {
        return days.includes(day.toString());
    }

    updateTimetable(event?: IPaginationEvent, filterValues?: ITimetableFilterValues): void {
        if (filterValues) {
            this.from = filterValues.from;
            this.to = filterValues.to;
            this.dateOfFlight = filterValues.dateOfFlight;
            this.aircraftType = filterValues.aircraftType;
            this.availableForMe = filterValues.availableForMe;
        }

        if (event) {
            this.page = event.page;
            this.pageSize = event.pageSize;
            this.router.navigate(
                [],
                {
                    relativeTo: this.route,
                    queryParams: event,
                    queryParamsHandling: 'merge'
                }
            )
        }

        this.service.getAvailableTrips({
            from: this.from,
            to: this.to,
            dateOfFlight: this.dateOfFlight,
            aircraftType: this.aircraftType,
            availableForMe: this.availableForMe
        }, this.page, this.pageSize).subscribe((timetable: ITimetable) => {
            this.timetable = timetable;
        })
    }

    checkTripIsAvailable(trip: IFlight): boolean {
        if (!this.logbook) {
            return false;
        }

        let availableRatings: Array<string> = [];

        if (this.logbook.typeRatings) {
            this.logbook.typeRatings.forEach((rating: ITypeRating) => {
                rating.aircraftTypes.forEach((type: IAircraftType) => {
                    availableRatings.push(type.icao);
                })
            })
        }

        return trip.from === this.logbook.location && availableRatings.includes(trip.aircraftType)
    }

    ngOnInit(): void {
        const params = this.route.snapshot.queryParams
        this.page = params['page'] ? parseInt(params['page']) : this.page;
        this.pageSize = params['pageSize'] ? parseInt(params['pageSize']) : this.pageSize;

        this.logbookService.getLogbook().subscribe((logbook: LogbookInterface) => {
            this.logbook = logbook

            this.logbook.typeRatings?.forEach((rating: ITypeRating) => {
                rating.aircraftTypes.forEach((type: IAircraftType)=> {
                    this.pilotAircraftTypes.push(type);
                })
            })
        })
    }

    public openDoBookingModal(content: any, bookingFlightId: number): void {
        this.bookingFlight = this.timetable?.items.find((flight: IFlight) => flight.id === bookingFlightId);
        this.selectedAircraftType = this.bookingFlight?.aircraftType;
        const departureTimeArray: Array<string> | undefined = this.bookingFlight?.etd.split(':');

        if (!departureTimeArray) {
            return;
        }

        this.selectedDepartureTime = { hour: parseInt(departureTimeArray[0]), minute: parseInt(departureTimeArray[1]) };
        this.doBookingModal = this.modalService.open(content, {ariaLabelledBy: 'do booking', centered: true});
    }

    public closeModal(): void {
        this.selectedDepartureTime = undefined;
        this.selectedAircraftType = undefined;
        this.bookingFlight = undefined;
        this.doBookingModal?.close('Cancel');
    }

    doBooking(id: number): void {
        if (!this.selectedAircraftType || !this.bookingFlight) {
            throw new Error("Aircraft Type should be selected first.");
        }

        this.bookingService.doBooking(this.bookingFlight?.id, this.selectedAircraftType, `${this.selectedDepartureTime.hour}:${this.selectedDepartureTime.minute}`).subscribe(
            () => {
                this.router.navigate(["dashboard", "active-trip"]);
                this.closeModal();
            },
            (err: any)=> {
                this.toastService.show('Невозможно забронировать рейс.', err.error.message);
                this.bookingDisabled = true
            });
    }
}
