export class MenuItem {
  constructor(private title: string, private link: string) {}

  public getTitle(): string
  {
    return this.title;
  }

  public getLink(): string
  {
    return this.link;
  }
}
