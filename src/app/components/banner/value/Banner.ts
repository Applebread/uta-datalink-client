export interface IActionBannerButton {
  title: string
  link: string
}

export class Banner {
  private active: boolean = false;

  constructor(
    private image: string,
    private title: string,
    private description: string,
    private button: IActionBannerButton | null = null
  ) {}
  getImage(): string {
    return this.image;
  }
  getTitle(): string {
    return this.title;
  }
  getDescription(): string {
    return this.description;
  }

  public isActive(): boolean {
    return this.active;
  }

  public markActive(): void {
    this.active = true;
  }

  public markInactive(): void {
    this.active = false;
  }

  public getButton(): IActionBannerButton | null {
    return this.button;
  }
}
