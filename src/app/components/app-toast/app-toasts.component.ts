import { Component } from '@angular/core';
import {AppToastService} from "../../services/toast/app.toast.service";

@Component({
  selector: 'app-toasts',
  templateUrl: './app-toasts.component.html',
  styleUrls: ['./app-toasts.component.scss']
})
export class AppToastsComponent {
  constructor(public toastService: AppToastService) {}
}
