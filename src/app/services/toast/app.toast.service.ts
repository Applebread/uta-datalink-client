import {Injectable} from "@angular/core";

export interface ToastInfo {
    header: string;
    body: string;
    classname: string;
    delay?: number;
}

export enum ToastType {
    default = 'default',
    success = 'success',
    danger = 'danger',
}

@Injectable({providedIn: 'root'})
export class AppToastService {
    toasts: ToastInfo[] = [];

    show(header: string, body: string, type: ToastType = ToastType.default) {
        let classname = '';
        switch (type) {
            case ToastType.danger:
                classname = 'bg-danger text-light'
                break;
            case ToastType.success:
                classname = 'bg-success text-light'
                break;
        }

        this.toasts.push({header, body, classname});
    }

    remove(toast: ToastInfo) {
        this.toasts = this.toasts.filter(t => t != toast);
    }
}
