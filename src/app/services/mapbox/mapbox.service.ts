import {Inject, Injectable} from "@angular/core";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";
import * as mapboxgl from "mapbox-gl";

export interface IMapLegPoint {
    latitude: number;
    longitude: number;
}

export interface IMapLeg {
    from: IMapLegPoint;
    to: IMapLegPoint;
}

@Injectable({
    providedIn: 'root'
})
export class MapboxService {
    map: mapboxgl.Map | undefined;

    constructor(@Inject(APP_CONFIG) private config: AppConfig) {
    }

    initializeMap(legs: Array<IMapLeg>): void {
        (mapboxgl as any).accessToken = this.config.mapboxApiKey;
        this.map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [
                legs[0].from.longitude,
                legs[0].from.latitude
            ],
            zoom: 5,
        });

        this.map.on('load', () => {
            this.updateMapMarkersAndRoute(legs);
        });
    }

    public updateMapMarkersAndRoute(legs: Array<IMapLeg>): void {
        if (!legs.length || !this.map) return;

        const route = legs.map((leg: IMapLeg) => [
            leg.from.longitude,
            leg.from.latitude,
        ]).concat([
            [
                legs[legs.length - 1].to.longitude,
                legs[legs.length - 1].to.latitude,
            ],
        ]);

        route.forEach(([lng, lat]) => {
            new mapboxgl.Marker().setLngLat([lng, lat]).addTo(this.map!);
        });

        if (route.length > 1) {
            const line: GeoJSON.FeatureCollection<GeoJSON.Geometry> = {
                type: 'FeatureCollection',
                features: [
                    {
                        type: 'Feature',
                        geometry: {
                            type: 'LineString',
                            coordinates: legs.map(leg => [
                                [leg.from.longitude, leg.from.latitude],
                                [leg.to.longitude, leg.to.latitude],
                            ]).flat(),
                        },
                        properties: {},
                    },
                ],
            };

// Проверяем, существует ли источник, и обновляем его или добавляем новый
            const source = this.map.getSource('route');
            if (source && (source as mapboxgl.GeoJSONSource).setData) {
                (source as mapboxgl.GeoJSONSource).setData(line);
            } else {
                this.map.addSource('route', {
                    type: 'geojson',
                    data: line,
                });
            }

// Добавляем слой линии, если его еще нет
            if (!this.map.getLayer('route-line')) {
                this.map.addLayer({
                    id: 'route-line',
                    type: 'line',
                    source: 'route',
                    layout: {
                        'line-join': 'round',
                        'line-cap': 'round',
                    },
                    paint: {
                        'line-color': '#000000', // Цвет линии
                        'line-width': 4,         // Толщина линии
                    },
                });
            }
        }

        // Вычисляем границы для всех точек маршрута
        const bounds = new mapboxgl.LngLatBounds();

        legs.forEach(leg => {
            bounds.extend([leg.from.longitude, leg.from.latitude]);
            bounds.extend([leg.to.longitude, leg.to.latitude]);
        });

        // Устанавливаем границы карты
        this.map.fitBounds(bounds, {
            padding: 50, // Добавляет отступы внутри карты
            maxZoom: 10, // Ограничивает максимальный зум
        });
    }
}
