import {Inject, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";
import {Observable} from "rxjs";

export interface ISimbriefFlightPlan {

}

export interface ISimbriefFlightRequest {
    departure: string
    arrival: string
    aircraft: string
    waypoints?: Array<string>
    date?: string
}

@Injectable({
    providedIn: 'root'
})
export class SimbriefService {

    constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {}

    public calculateFlightPlan(request: ISimbriefFlightRequest): Observable<ISimbriefFlightPlan> {
        return this.http.post<ISimbriefFlightPlan>(`${this.config.simbriefApiUrl}/flights`, request)
    }
}
