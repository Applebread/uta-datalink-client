import {AuthService} from "../auth/auth.service";
import {Injectable} from "@angular/core";

export interface IMenuItem {
    title: string;
    path: string;
    icon: string;
    availableForRoles: Array<string>
    queryParams?: any
}

@Injectable({
    providedIn: "root"
})
export class DashboardMenuService {
    private allItems: Array<IMenuItem> = [
        {
            title: "Статистика",
            path: "/dashboard/main",
            icon: "nc-chart-pie-36",
            availableForRoles: ["ROLE_USER", "ROLE_PILOT"]
        },
        {
            title: "Забронировать рейс",
            path: "/dashboard/trip-request",
            queryParams: {availableForMe: true},
            icon: "nc-compass-05",
            availableForRoles: ["ROLE_USER", "ROLE_PILOT"]
        },
        {
            title: "Центр обучения",
            path: "/dashboard/exams",
            icon: "nc-hat-3",
            availableForRoles: ["ROLE_USER", "ROLE_PILOT"]
        },
        {
            title: "Инструкторская",
            path: "/dashboard/instructor/trainings",
            icon: "nc-hat-3",
            availableForRoles: ["ROLE_INSTRUCTOR"]
        },
    ];

    constructor(private authService: AuthService) {
    }

    public availableMenuItems(): Array<IMenuItem> {
        const userRoles = this.authService.currentUserRoles();

        return this.allItems.filter((item: IMenuItem) => {
            return item.availableForRoles.every((role: string) => userRoles?.includes(role))
        })
    }
}
