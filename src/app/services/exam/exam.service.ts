import {HttpClient} from "@angular/common/http";
import {Inject, Injectable} from "@angular/core";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";
import {Observable} from "rxjs";


export interface IExamCollection {
  items: Array<IExam>;
  totalResults: number;
}

export interface IExam {
  id: number;
  description: string;
  type: string;
  date: string;
  isPassed: boolean;
  passedAt: string | null;
  aircraftType?: string;
  questions?: Array<IQuestion>;
  examResult?: IExamResult
  instructorName?: string
}

export interface IQuestion {
  id: number;
  question: string;
  onlyOneAnswerIsPossible: boolean;
  options: Array<IQuestionOption>
}

export interface IQuestionOption {
  id: number;
  text: string;
  checked: boolean;
}

export interface IExamResult {
  isSuccess: boolean;
  resultPercent: number;
}

export enum ExamType {
  Minimum = 'minimum',
  TypeRating = 'type_rating',
  Training = 'training',
}

@Injectable({
  providedIn: 'root'
})
export class ExamService {
  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
  }

  public getExamList(): Observable<IExamCollection> {
    return this.http.get<IExamCollection>(this.config.apiEndpoint + '/api/pilot/exams');
  }

  public getExamDetails(id: number): Observable<IExam> {
    return this.http.get<IExam>(this.config.apiEndpoint + '/api/exam/' + id);
  }

  public makeAnswer(questionId: number, selectedOptionsIds: Array<number>): Observable<void> {
    return this.http.post<void>(this.config.apiEndpoint + '/api/pilot/exams/'+ questionId + '/answer', { answerOptionsIds: selectedOptionsIds });
  }

  public checkExam(examId: number): Observable<IExamResult> {
    return this.http.post<IExamResult>(this.config.apiEndpoint + `/api/pilot/exams/${examId}/check`, {});
  }

  public reassignFailedExam(examId: number): Observable<void> {
    return this.http.post<void>(this.config.apiEndpoint + `/api/pilot/exams/${examId}/reassign`, {});
  }

  public requestExam(examType: ExamType, typeRatingId?: number, minimumId?: number): Observable<IExam> {
    return this.http.post<IExam>(this.config.apiEndpoint + '/api/pilot/exams/request', {examType, typeRatingId, minimumId});
  }

  public isExamDateApplicable(exam: IExam): boolean {
    const examDate = new Date(exam.date);
    return new Date() >= examDate;
  }
}
