import {Inject, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";

export interface IAircraftTypes {
  totalResults: number,
  items: Array<IAircraftType>
}

export interface IAircraftList {
  totalResults: number,
  items: Array<IAircraft>
}

export interface IAircraftType {
  "id"?: Number,
  "icao": string,
  "name": string,
  "description": string,
  "image": string,
  "heavy": boolean
}

export interface IAircraft {
  "id"?: number,
  "icao": string
  "location": string
  "gate": string
  "registration": string
  "fob": number
  stats?: IAircraftStats
}

export interface IAircraftStats {
  lastFlightAt: string;
  acTotalFH: string;
  acTotalFC: number;
  latestFiftyCheckAt: string;
  latestTwoHundredCheckAt: string;
  latestThousandCheckAt: string;
}

export interface AircraftImageUploadStatus {
  status: string;
  filename: string;
}

@Injectable({
  providedIn: 'root'
})
export class AircraftService {
  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {
  }

  public aircraftTypes(page: number = 1, pageSize: number = 15): Observable<IAircraftTypes> {
    return this.http.get<IAircraftTypes>(this.config.apiEndpoint + "/api/admin/aircraft-types", {withCredentials: true});
  }
}
