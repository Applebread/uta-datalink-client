import {Inject, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";
import {Observable} from "rxjs";
import {AuthService} from "../auth/auth.service";
import {IAircraftType} from "../aircraft/aircraft.service";

export interface LogbookInterface {
  id?: number,
  email: string,
  name: string,
  flightTime?: string,
  birthDate?: string,
  avatar?: string,
  typeRatings?: Array<ITypeRating>,
  location?: string,
  minimum: string | null
  rank: string | null
  rankIcon: string | null
  logbookItems: Array<LogbookItemInterface> | null
}

export interface LogbookItemInterface {
  created: string,
  from: string,
  to: string,
  flightTime: string,
  landingAirport: string,
  route: string
}

export interface ITypeRating {
  id: number,
  name: string,
  aircraftTypes: Array<IAircraftType>
}

@Injectable({
  providedIn: 'root'
})
export class LogbookService {
  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig, private authService: AuthService) {
  }

  getLogbook(): Observable<LogbookInterface> {
    return this.http.get<LogbookInterface>(this.config.apiEndpoint + '/api/pilot/logbook');
  }

  getTypeRatingsList(): Observable<Array<ITypeRating>> {
    return this.http.get<Array<ITypeRating>>(this.config.apiEndpoint + '/api/pilot/type-ratings/available')
  }

  getLogbookFromLoggedUser(): LogbookInterface {
    if (!this.authService.loggedInUser) {
      throw new Error("No logged in user")
    }

    return {
      email: this.authService.loggedInUser.email,
      name: this.authService.loggedInUser.fullName,
      birthDate: '',
      minimum: null,
      rank: null,
      rankIcon: null,
      logbookItems: []
    }
  }
}
