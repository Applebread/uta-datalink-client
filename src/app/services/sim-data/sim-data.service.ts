import {Injectable} from '@angular/core';
import {from, Observable} from 'rxjs';

export interface ISimData {
    latitude: number;
    longitude: number;
    magVar: number;
    heading: number;
    bank: number;
    pitch: number;
    verticalSpeed: number;
    gForce: number;
    altitude: number;
    seaLevelAltitude: number;
    aircraftType: string;
    acTypeICAO: string;
    landingLights: boolean;
    seatbeltSign: boolean;
    fuelTotalQuantityWeightPounds: number;
    fuelTotalQuantityWeightKg: number;
}

@Injectable({
    providedIn: 'root'
})
export class SimDataService {

    constructor() {
    }

    getSimData(): Observable<ISimData> {
        // @ts-ignore
        return from(window.electronAPI.getFSUIPCData());
    }

    updateCurrentTrack(trackName: string, data: ISimData): void {
        // @ts-ignore
        window.electronAPI.appendToCsv(trackName, [{timestamp: new Date().toISOString(), ...data}]);
        console.log("Данные отправлены в CSV");
    }
}
