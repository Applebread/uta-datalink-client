import {Inject, Injectable} from '@angular/core';
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

export interface IRegistrationData {
  login: string,
  email: string,
  fullName: string,
  birthDate: string,
  password: string,
  confirmPassword: string
}

export interface RegistrationErrors {
  errors: RegistrationErrorCollection
}

export interface RegistrationErrorCollection {
  [key: string]: string
}

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) {}

  public register(data: IRegistrationData): Observable<void | RegistrationErrors> {
    return this.http.post<void | RegistrationErrors>(`${this.config.apiEndpoint}/api/auth/register`, data)
  }
}
