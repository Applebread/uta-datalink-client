import {Inject, Injectable} from '@angular/core';

import {HttpClient, HttpResponse} from "@angular/common/http";
import {APP_CONFIG, AppConfig} from "../../app-config/app-config.module";
import {Router} from "@angular/router";
import {Observable} from "rxjs";

interface ILoggedInUser {
  login: string,
  email: string,
  fullName: string,
  roles: Array<string>
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig, private router: Router) {
    this.isLoggedIn();
  }

  isUserLoggedIn: boolean = false;
  loggedInUser?: ILoggedInUser;

  login(username: string, password: string, withoutRedirect: boolean = false): Observable<any> {
    const observable = this.http.post<any>(this.config.apiEndpoint + '/api/auth', {username, password}, {
      observe: 'response',
      withCredentials: true
    })

    observable.subscribe(async (user: any) => {
      localStorage.setItem('token', user.body.token);

      await this.updateAuthInfo(withoutRedirect);
    }, (err: any) => {
      this.loggedInUser = undefined;

      localStorage.removeItem('token');
      localStorage.removeItem('loggedInUser');
    })

    return observable;
  }

  public async updateAuthInfo(withoutRedirect: boolean): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http.get<ILoggedInUser>(this.config.apiEndpoint + '/api/auth/info', {
        observe: 'response',
        withCredentials: true,
        headers: {Authorization: `Bearer ${localStorage.getItem('token')}`}
      }).subscribe(async (user: HttpResponse<ILoggedInUser>) => {
        this.loggedInUser = user.body ? user.body : undefined;
        localStorage.setItem('loggedInUser', JSON.stringify(this.loggedInUser));

        if (!withoutRedirect) {
          await this.router.navigate(['/', 'dashboard', 'main']);
        }

        resolve();
      }, (err) => {
        reject(err);
      });
    });

  }

  currentUserRoles(): Array<string> | null {
    if (!this.isLoggedIn()) {
      return null;
    }

    return this.loggedInUser?.roles || null;
  }

  isLoggedIn(): boolean {
    this.isUserLoggedIn = !!localStorage.getItem('loggedInUser') && !!localStorage.getItem('token');

    const storedUser: string | null = localStorage.getItem('loggedInUser');
    this.loggedInUser = storedUser ? JSON.parse(storedUser) : undefined;

    return this.isUserLoggedIn;
  }

  logout(): void {
    this.loggedInUser = undefined;
    localStorage.removeItem('token');
    localStorage.removeItem('loggedInUser');
  }
}
