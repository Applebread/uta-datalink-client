import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TripRequestComponent} from "./views/dashboard/trip-request/trip-request.component";
import {TripBookingComponent} from "./views/dashboard/trip-booking/trip-booking.component";
import {LoginComponent} from './layout/dashboard/login/login.component';
import {AppGuardGuard} from './app-guard.guard';

const routes: Routes = [
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {path: 'login', component: LoginComponent},
    {path: 'trip-request', component: TripRequestComponent, canActivate: [AppGuardGuard]},
    {path: 'active-trip', component: TripBookingComponent, canActivate: [AppGuardGuard]},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
