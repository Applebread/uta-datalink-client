import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HeaderComponent} from './layout/public/header/header.component';
import {MainMenuComponent} from './components/main-menu/main-menu.component';
import {BannerComponent} from './components/banner/banner.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AppConfigModule} from "./app-config/app-config.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DashboardHeaderComponent} from './layout/dashboard/dashboard-header/dashboard-header.component';
import {DashboardFooterComponent} from './layout/dashboard/dashboard-footer/dashboard-footer.component';
import {DashboardMainComponent} from './layout/dashboard/dashboard-main/dashboard-main.component';
import {DashboardComponent} from './views/dashboard/dashboard/dashboard.component';
import {DashboardSidebarComponent} from './layout/dashboard/dashboard-sidebar/dashboard-sidebar.component';
import {TripRequestComponent} from './views/dashboard/trip-request/trip-request.component';
import {AuthTokenInterceptor} from "./interceptors/authToken.Interceptor";
import {DashboardUserComponent} from './views/dashboard/dashboard/dashboard-user/dashboard-user.component';
import {
    DashboardSmallBlockComponent
} from './views/dashboard/dashboard/dashboard-small-block/dashboard-small-block.component';
import {TypeRatingBlockComponent} from './views/dashboard/dashboard/type-rating-block/type-rating-block.component';
import {AppToastsComponent} from './components/app-toast/app-toasts.component';
import {PagableTableComponent} from './components/pagable-table/pagable-table.component';
import {TimetableComponent} from './views/dashboard/trip-request/timetable/timetable.component';
import {
    TimetableFilterComponent
} from './views/dashboard/trip-request/timetable/timetable-filter/timetable-filter.component';
import {TableColumnDirective} from './components/pagable-table/table-column.directive';
import {AutocompleteComponent} from './components/autocomplete/autocomplete.component';
import {TripBookingComponent} from './views/dashboard/trip-booking/trip-booking.component';
import {NgbDropdown} from "@ng-bootstrap/ng-bootstrap";
import {
    AircraftStatsModalComponent
} from './views/dashboard/trip-booking/aircraft-stats-modal/aircraft-stats-modal.component';
import {LegsManagementComponent} from './views/dashboard/trip-booking/legs-management/legs-management.component';
import {TripBookingMapComponent} from './views/dashboard/trip-booking/trip-booking-map/trip-booking-map.component';
import {TripLegComponent} from './views/dashboard/trip-booking/legs-management/trip-leg/trip-leg.component';
import {
    TripLegSelectDestinationModalComponent
} from './views/dashboard/trip-booking/legs-management/trip-leg/trip-leg-select-destination-modal/trip-leg-select-destination-modal.component';
import {
    FlightInformationComponent
} from './views/dashboard/trip-booking/flight-information/flight-information.component';
import {
    AircraftInformationComponent
} from './views/dashboard/trip-booking/aircraft-information/aircraft-information.component';
import {LoginComponent} from './layout/dashboard/login/login.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        MainMenuComponent,
        BannerComponent,
        DashboardHeaderComponent,
        DashboardFooterComponent,
        DashboardMainComponent,
        DashboardComponent,
        DashboardSidebarComponent,
        TripRequestComponent,
        DashboardUserComponent,
        DashboardSmallBlockComponent,
        TypeRatingBlockComponent,
        AppToastsComponent,
        PagableTableComponent,
        TimetableComponent,
        TimetableFilterComponent,
        TableColumnDirective,
        AutocompleteComponent,
        TripBookingComponent,
        AircraftStatsModalComponent,
        LegsManagementComponent,
        TripBookingMapComponent,
        TripLegComponent,
        TripLegSelectDestinationModalComponent,
        FlightInformationComponent,
        AircraftInformationComponent,
        LoginComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgbModule,
        HttpClientModule,
        AppConfigModule,
        ReactiveFormsModule,
        FormsModule,
        NgbDropdown
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthTokenInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
