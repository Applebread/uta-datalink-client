// login.component.ts (Angular Login Component)
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {ISimData, SimDataService} from '../../../services/sim-data/sim-data.service';
import {AuthService} from '../../../services/auth/auth.service';
import {AppToastService, ToastType} from '../../../services/toast/app.toast.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    username = '';
    password = '';
    errorMessage = '';
    fsData?: ISimData;

    constructor(private http: HttpClient, private router: Router, private simData: SimDataService, private auth: AuthService, private toasts: AppToastService) {}

    ngOnInit(): void {
        this.checkSimData();
    }

    checkSimData(): any {
        let i = 0;
        setInterval(() => {
            this.simData.getSimData().subscribe((data: ISimData) => {
                this.simData.updateCurrentTrack('test', data);
            });
        }, 3000)
    }

    login() {
        this.auth.login(this.username, this.password).subscribe((data) => {
            this.router.navigate(['trip-request']);
        }, (error) => {
            this.toasts.show('Authorization error', error.error.message, ToastType.danger);
        });
    }
}
