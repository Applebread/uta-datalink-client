const { app, BrowserWindow, ipcMain, Tray, Menu } = require('electron');
const path = require('path');
const fs = require('fs');
const { parse } = require('json2csv');

let mainWindow;
let tray = null;

const gotTheLock = app.requestSingleInstanceLock();

if (!gotTheLock) {
    app.quit();
} else {
    app.on('second-instance', (event, commandLine, workingDirectory) => {
        console.log("Попытка запуска второго экземпляра!");
        if (mainWindow) {
            if (mainWindow.isMinimized()) mainWindow.restore();
            mainWindow.focus();
        }
    });

    app.whenReady().then(() => {
        createMainWindow();
    });
}

function createMainWindow() {
    app.whenReady().then(() => {
        mainWindow = new BrowserWindow({
            width: 600,
            height: 800,
            autoHideMenuBar: true,
            resizable: false,
            title: "UTairDLC",
            icon: path.join(__dirname, 'public', 'favicon.png'),
            webPreferences: {
                nodeIntegration: true,
                contextIsolation: true,
                preload: path.join(__dirname, 'preload.js')
            }
        });

        mainWindow.loadFile(path.join(__dirname, 'dist/uta-datalink-client/index.html'));

        // if (app.isPackaged) {
        //     mainWindow.removeMenu();
        //     mainWindow.webContents.on("before-input-event", (event, input) => {
        //         if (input.control && input.shift && input.key.toLowerCase() === 'i') {
        //             console.log("DevTools заблокированы в продакшене!");
        //             event.preventDefault();
        //         }
        //     });
        // }

        tray = new Tray(path.join(__dirname, 'public', 'favicon.png'));
        const contextMenu = Menu.buildFromTemplate([
            {label: 'Открыть', click: () => mainWindow.show()},
            {label: 'Выход', click: () => {
                    if (mainWindow) {
                        mainWindow.destroy();
                    }
                    app.quit();
                }
            }
        ]);

        tray.setContextMenu(contextMenu);
        tray.setToolTip('UTairDLC');
        tray.on('click', () => {
            if (mainWindow) {
                if (mainWindow.isMinimized()) {
                    mainWindow.show();
                }

                mainWindow.focus();
            }
        });

        mainWindow.on('minimize', (event) => {
            event.preventDefault();
            mainWindow.minimize();
        })

        mainWindow.on('close', (event) => {
            event.preventDefault();
            mainWindow.hide();
        });
    });
}

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

ipcMain.on('append-to-csv', (event, filename, data) => {
    const logsDir = path.join(__dirname, 'logs'); // Папка logs
    const csvFilePath = path.join(logsDir, filename + '.csv'); // Файл

    try {
        if (!fs.existsSync(logsDir)) {
            fs.mkdirSync(logsDir, { recursive: true });
            console.log("Папка 'logs' создана");
        }

        const fileExists = fs.existsSync(csvFilePath);
        const csvData = parse(data, { header: !fileExists });
        fs.appendFileSync(csvFilePath, csvData + "\n", 'utf8');

        console.log("Данные отправлены в файл:", csvFilePath);
    } catch (error) {
        console.error("Ошибка при записи в CSV", error);
    }
});

